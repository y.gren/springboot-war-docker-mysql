package com.example.springbootwardockermysql.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UserController2 {

    @RequestMapping("/res")
    public String result() {
        return "result";
    }

    @RequestMapping("/res2")
    @ResponseBody
    public String result2() {
        return "result";
    }
}

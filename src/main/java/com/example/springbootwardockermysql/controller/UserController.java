package com.example.springbootwardockermysql.controller;

import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    @GetMapping("/test")
    public String all() {
        return "HelloWorld IT MEEEEE!!!!!! Test";
    }

    @RequestMapping("/")
    public String helloWorld() {
        return "Hey, Im running";
    }

    @RequestMapping("/result")
    public String result() {
        return "result";
    }

    @RequestMapping("/result2")
    @ResponseBody
    public String result2() {
        return "result";
    }

    @RequestMapping(value = "/result3", method = RequestMethod.GET)
    public String index() {
        return "result";
    }

}

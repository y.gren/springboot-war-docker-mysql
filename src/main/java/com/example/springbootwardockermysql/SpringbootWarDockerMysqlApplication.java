package com.example.springbootwardockermysql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootWarDockerMysqlApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootWarDockerMysqlApplication.class, args);
    }

}

FROM tomcat:8.5-jre8
COPY target/springboot-war-docker-mysql.war /usr/local/tomcat/webapps/
CMD ["catalina.sh", "run"]